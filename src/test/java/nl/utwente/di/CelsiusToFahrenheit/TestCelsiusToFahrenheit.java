package nl.utwente.di.CelsiusToFahrenheit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/** * Tests theQuoter */
public class TestCelsiusToFahrenheit
{
    @Test
    public void testBook1() throws Exception {
    CelsiusToFahrenheit celsiusToFahrenheit = new CelsiusToFahrenheit();
    double price= celsiusToFahrenheit.getFahrenheit(10);
    Assertions.assertEquals(50.0, price, 0.1, "10 celcius to fahrenheit" );
}
}