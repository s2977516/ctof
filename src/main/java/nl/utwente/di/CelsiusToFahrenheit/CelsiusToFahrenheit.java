package nl.utwente.di.CelsiusToFahrenheit;

public class CelsiusToFahrenheit
{
    double getFahrenheit(double celcius)
    {

        return (celcius * (9/5)) + 32;
    }
}
